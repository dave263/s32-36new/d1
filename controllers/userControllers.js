const CryptoJS = require("crypto-js");
// const bcrypt = require('bcrypt');
const User = require('./../models/Users')

const Course = require('./../models/Course')

//import createToken function from auth module
const {createToken} = require('./../auth');


//REGISTER A USER
module.exports.register = async (reqBody) => {
	// console.log(reqBody)
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		// password: bcrypt.hashSync(password, 10)
	})

	return await  newUser.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


//GET ALL USERS
module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}


//CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
	const {email} = reqBody

	return await User.findOne({email: email}).then((result, err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}



//LOGIN A USER
module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if pw is correct, decrypt to compare pw input from the user from pw stored in db
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password == decryptedPw) //true
				

				if(reqBody.password == decryptedPw){
					//create a token for the user
					return { token: createToken(result) }
				} else {
					return {auth: `Auth Failed!`}
				}

			} else {
				return err
			}
		}
	})
}





//RETRIEVE USER INFORMATION
module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})
}



// UPDATE USER INFO
module.exports.update = async (userId, reqBody) => {
	const userData = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(userId, {$set: userData}, {new:true}).then((result, err) => {
		// console.log(result)
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}




// UPDATE PASSWORD
module.exports.updatePw = async (id, password) => {
	let updatedPw = {
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(id, {$set: updatedPw})
	.then((result, err) => {
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}



//CHANGE TO ADMIN STATUS TO TRUE
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}



//CHANGE TO ADMIN STATUS TO FALSE
module.exports.userStatus = async (reqBody) => {

	return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}}).then((result, err) => result ? true : err)
}




//DELETE A USER
module.exports.deleteUser = async (reqBody) => {
	const {email} = reqBody
	return await User.findOneAndDelete({email: email}).then((result, err) => result ? true : err)
}

// ENROLL A USER
module.exports.enroll = async (data) => {
	const {userId, courseId} = data
	const updateUser = await User.findById(userId).then(result => {
		// console.log(result)
		result.enrollments.push({courseId: courseId})

		return result.save().then(user => user ? true : false )
	})

	const updatedCourse = await Course.findById(courseId).then(result => {
		result.enrollees.push({userId: userId})

		return result.save().then(course => course ? true : false )
	})

	if(updatedUser && updatedCourse) {
		return true
	} else {
		false
	}
}