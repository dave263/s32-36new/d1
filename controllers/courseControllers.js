const Course = require('./../models/Course');


//CREATE A COURSE
module.exports.create = async (reqBody) => {
	const {courseName, description, price} = reqBody

	let newCourse = new Course({
		courseName: courseName,
		description: description,
		price: price
	})
	// console.log(newCourse)
	return await newCourse.save().then((result, err) => result ? result : err)
}




//GET ALL COURSES
module.exports.getAllCourses = async () => {
// console.log('h')
	return await Course.find().then(result => result)
}




//ACTIVE COURSES
module.exports.activeCourses = async () => {

	return await Course.find({isOffered:true}).then(result => result)
}




//GET SPECIFIC COURSE
module.exports.getACourse = async (id) => {

	return await Course.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Course not found`}
			}else{
				return err
			}
		}
	})
}




//UPDATE A COURSE
module.exports.updateCourse = async (courseId, reqBody) => {

	return await Course.findByIdAndUpdate(courseId, {$set: reqBody}, {new:true}).then(result => result)
}




//ARCHIVE COURSE
module.exports.archive = async (courseId) => {

	return await Course.findByIdAndUpdate(courseId, {$set: {isOffered: false}}, {new:true}).then(result => result)
}




//UNARCHIVE COURSE
module.exports.unArchive = async (courseId) => {

	return await Course.findByIdAndUpdate(courseId, {$set: {isOffered: true}}, {new:true}).then(result => result)
}



//DELETE A COURSE
module.exports.deleteCourse = async (courseId) => {

	return await Course.findByIdAndDelete(courseId).then((result, err) => result ? true : err)
}
