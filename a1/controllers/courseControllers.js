const CryptoJS = require("crypto-js");

const User = require('./../models/User')

const Course = require('./../models/Course')

const {createToken} = require('./../auth');


//Create a route /create to create a new course, save the course in DB and return the document
    //Only admin can create a course
module.exports.createCourse = async (reqBody) => {

	const {courseName, description, price, isOffered, enrollees} = reqBody

	const newCourse = new Course({
		courseName: courseName,
		description: description, 
		price: price,
		isOffered: isOffered,
		enrollees: enrollees
	})

	return await  Course.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


//Create a route "/" to get all the courses, return all documents found
    //both admin and regular user can access this route
module.exports.getAllCourses = async () => {

	return await Course.find().then(result => result)
}




//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
    //both admin and regular user can access this route
module.exports.course = async (id) => {

	return await Course.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Course does not exist`}
			} else {
				return err
			}
		}
	})
}



//Create a route "/:courseId/update" to update course info, return the updated document
    //Only admin can update a course
module.exports.updateCourse = async (courseId, reqBody) => {
	const courseInfo = {
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price,
		isOffered: reqBody.isOffered,
		enrollees: reqBody.enrollees
	}

	return await Course.findByIdAndUpdate(courseInfo, {$set: courseInfo}, {new:true}).then((result, err) => {
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}





//Create a route "/isActive" to get all active courses, return all documents found
    //both admin and regular user can access this route
module.exports.activeCourse = async (reqBody) => {
	const {courseName} = reqBody

	return await User.findOne({courseName: courseName}, {$set: {isOffered: true}}).then((result, err) => result ? true : err)
}


//Create a route "/:courseId/archive" to update isActive to false, return updated document
    //Only admin can update a course
module.exports.activeCourse = async (reqBody) => {

	return await User.findOneAndUpdate({courseName: reqBody.courseName}, {$set: {isOffered: false}}).then((result, err) => result ? true : err)
}


//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
    //Only admin can update a course
module.exports.activeCourse = async (reqBody) => {

	return await User.findOneAndUpdate({courseName: reqBody.courseName}, {$set: {isOffered: true}}).then((result, err) => result ? false : err)
}



//Create a route "/:id/delete-course" to delete a courses, return true if success
    //Only admin can delete a course
module.exports.deleteCourse = async (reqBody) => {
	const {courseName} = reqBody
	return await User.findOneAndDelete({courseName: courseName}).then((result, err) => result ? true : err)
}
